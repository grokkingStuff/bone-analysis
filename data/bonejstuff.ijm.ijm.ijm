run("Area/Volume fraction", "inputimage=net.imagej.ImgPlus@f9e8a9");
run("Anisotropy", "inputimage=net.imagej.ImgPlus@f9e8a9 directions=2000 lines=10000 samplingincrement=1.73 recommendedmin=true printradii=false printeigens=false displaymilvectors=false");
