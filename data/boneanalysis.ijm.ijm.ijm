//open("/Users/grokkingstuff/Sync/work/bone_analysis/data/pre_load_gauss_otsu.mha");
run("16-bit");
run("Auto Threshold", "method=Otsu white stack use_stack_histogram");
run("Area/Volume fraction", "inputimage=net.imagej.ImgPlus@54e5a2bd");
run("Particle Analyser", "surface_area enclosed_volume moments euler thickness ellipsoids min=0.000 max=Infinity surface_resampling=2 surface=Gradient split=0.000 volume_resampling=2");
run("Summarize");
//saveAs("Results", "/Users/grokkingstuff/Sync/work/bone_analysis/data/pre_load_gauss_otsu_particle_analyzer.csv");